<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//user routes
Route::get('/user',[\App\Http\Controllers\UserController::class,'index']);
//Route::post('/user/create',[\App\Http\Controllers\UserController::class,'userCreate'])->name('userCreate');
Route::get('/user/create_page',[\App\Http\Controllers\UserController::class,'userCreatePage'])->name('userCreatePage');
Route::get('/user/edit/{id}',[\App\Http\Controllers\UserController::class,'userEdit'])->name('userEdit');
Route::post('/user/update/{id}',[\App\Http\Controllers\UserController::class,'userUpdate'])->name('userUpdate');
Route::get('/user/list',[\App\Http\Controllers\UserController::class,'userList'])->name('userList');
Route::get('/user/order-payments/{id}',[\App\Http\Controllers\UserController::class,'userOrderPaymentList'])->name('userOrderPaymentList');

//payment routes
Route::get('/payment/list',[\App\Http\Controllers\PaymentController::class,'payList'])->name('payList');


//order routes
Route::get('/order/list',[\App\Http\Controllers\OrderController::class,'orderList'])->name('orderList');

//generate CSV file
Route::get('/export/csv',[\App\Http\Controllers\CsvController::class,'exportCSV'])->name('exportCSV');
Route::post('/import/csv',[\App\Http\Controllers\CsvController::class,'importCSV'])->name('importCSV');
