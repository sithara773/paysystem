<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    protected $fillable = ['amount', 'payment_method', 'payment_date', 'order_id'];

    public function order()
    {
        return $this->belongsTo(order::class);
    }
}
