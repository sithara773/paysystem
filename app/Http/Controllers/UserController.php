<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Payment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Toastr;

class UserController extends Controller
{
    public function index()
    {
        return view('dashboard.userDashboard');
    }

    public function userList()
    {
        $user = User::all();
        if ($user) {
            $data = [];
            $data['users'] = $user;
            return view('user.userList', $data);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Selected Users not Found']);
        }
    }

    public function userCreatePage()
    {
        return view('user.userCreate');
    }


    public function userEdit($id)
    {
        try {
            $select_user = User::findOrFail($id);
            $order_list=Order::with('Payments')->where('user_id',$id)->get();
            if ($select_user || $order_list) {
                $data = [];
                $data['user'] = $select_user;
                $data['orders'] = $order_list;

                return view('user.userCreate', $data);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Selected Users not Found']);
            }
        } catch (\Exception $e) {
//            dd($e);
        }

    }

    public function userUpdate(Request $request, $id)
    {
//        dd($request);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'nic' => 'required|string|max:255',
            'gender' => 'required|string|max:255',
            'contact' => ['required', 'regex:/^(?:\+94|0)[0-9]{9,}$/'],
            'email' => 'required|string|email',
            'no' => 'required|string|max:255',
            'street' => 'required|string|max:255',
            'city' => 'required|string|max:255',
        ]);
        if ($validator->passes()) {
            try {
                $user = User::findOrFail($id);
                $user->name = $request->name;
                $user->lname = $request->lname;
                $user->nic = $request->nic;
                $user->gender = $request->gender;
                $user->contact = $request->contact;
                $user->email = $request->email;
                $user->no = $request->no;
                $user->street = $request->street;
                $user->city = $request->city;
                $user->save();
                return back()->with('success', 'Successfully Updated User Details');
            } catch (\Exception $e) {
                return back()->withInput()->with('error', 'Something went wrong');
            }
        } else {
            return back()->withInput()->withErrors($validator->errors());
        }
    }

    public function userOrderPaymentList($id)
    {
        try {
            $payment_list=Payment::with('Payments')->where('order_id',$id)->get();

            if ($payment_list) {
                $data = [];
                $data['payments'] = $payment_list;

                return view('user.userCreate', $data);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Selected Users not Found']);
            }
        } catch (\Exception $e) {
//            dd($e);
        }
    }
}
