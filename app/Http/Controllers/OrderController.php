<?php

namespace App\Http\Controllers;


use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function orderList()
    {
        $orders = Order::with('user')->get();
//        dd($orders);
        if ($orders) {
            $data = [];
            $data['orders'] = $orders;
            return view('order.orderList', $data);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Selected Users not Found']);
        }
    }
}
