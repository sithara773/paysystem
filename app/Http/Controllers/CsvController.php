<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvController extends Controller
{
    public function exportCSV()
    {

        $payments = Payment::with('order')->get();
        if (!($payments->isEmpty())) {
            $data = [];
            $data[] = ['Amount', 'Payment_Method', 'Payment_Date', 'Order_Id'];
//        $data[]=['ID','Amount','User',Payment_Method','Payment_Date','Order_Id'];
            foreach ($payments as $payment) {
                $data[] = [
//                $payment->id,
                    $payment->amount,
//                $payment->order->user->name,
                    $payment->payment_method,
                    $payment->payment_date,
                    $payment->order_id,
                ];
            }

            $file = fopen('payments', 'w');
            foreach ($data as $line) {
                fputcsv($file, $line);
            }
            fclose($file);

            $headers = array(
                'Content-Type' => 'text/csv',
            );
            return Response::download('payments', 'payments.csv', $headers);


        } else {
            return redirect()->back()->with('error', 'Something went wrong. Nothing to export!');
        }

    }

    public function importCSV(Request $request)
    {

        if ($request->file('file')) {
            $file = $request->file('file');
            $extension = $file->getClientOriginalExtension();//edited after send email
            if ($extension === 'csv') {//edited after send email
                $fileContents = file($file->getPathname());

                $inserted = 0;
                $notInserted = -1;

                foreach ($fileContents as $line) {
                    $data = str_getcsv($line);
//                dd($data);

                    $amount = doubleval($data[0]);
                    $paymentMethod = $data[1];
                    $date = trim($data[2]);
                    $order_id = intval($data[3]);
                    $formattedDate = null;

                    if (empty($amount) || empty($paymentMethod) || empty($date) || empty($order_id) || $order_id === 0) {
                        $notInserted++;
                        continue;
                    }

                    // date format
                    $dateObj = \DateTime::createFromFormat('m/d/Y', $date);
                    if ($dateObj) {
                        $formattedDate = $dateObj->format('Y-m-d');
                    } else {
                        $notInserted++;
                        continue;
                    }

                    // adding payments
                    $result = Payment::create([
                        'amount' => $amount,
                        'payment_method' => $paymentMethod,
                        'payment_date' => $formattedDate,
                        'order_id' => $order_id,
                    ]);

                    if ($result) {
                        $inserted++;
                    } else {
                        $notInserted++;
                    }
                }

                if ($inserted === 0) {  //edited after send email
                    $message = 'Something went wrong imported CSV file. ';
                    $message .= 'Inserted: ' . $inserted . ', Not Inserted: ' . $notInserted;

                    return redirect()->back()->with('error', $message);
                } else {
                    $message = 'CSV file imported successfully. ';
                    $message .= 'Inserted: ' . $inserted . ', Not Inserted: ' . $notInserted;

                    return redirect()->back()->with('success', $message);
                }

            } else {
                return redirect()->back()->with('error', 'Something went wrong. This is not a CSV File!');
            }

        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please Select CSV File!');
        }
    }


}
