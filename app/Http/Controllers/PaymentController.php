<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function payList()
    {
        $payments = Payment::with('order')->get();
//            dd($payments);
        if ($payments) {
            $data = [];
            $data['payments'] = $payments;
            return view('payment.paymentList', $data);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Selected Users not Found']);
        }
    }
}
