@extends('layouts.frontend')

@section('content')

    <h2 class="d-flex justify-content-center">User List</h2>

    <div class="container" style="height: 500px; overflow-y: auto;">
        <table class="table table-hover table table-scroll">
            <thead style="width: auto">
            <tr>
                <th>ID</th>
                <th>FirstName</th>
                <th>LastName</th>
                <th>Email</th>
                <th>Contact</th>
                <th>Gender</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody >
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->lname}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->contact}}</td>
                    <td>{{$user->gender}}</td>
                    <td>
                        <a type="button"  href="{{route('userEdit',['id'=>$user->id])}}" title="{{$user->name}}" class="btn btn-primary form-control" >Edit</a>
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection
