@extends('layouts.frontend')

@php
    if (request()->routeIs('userCreate')){
   $url = route('userCreate');
    }else{
    $url = route('userUpdate', ['id'=>@$user['id']]);
    }
@endphp

@section('content')
    <form action="{{$url}}" method="POST" autocomplete="false" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-12">
                <h4 class="mb-1 mt-2">
                    <i data-feather="user" class="font-medium-4 mr-25"></i>
                    <span class="align-middle">User Information</span>
                </h4>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label class="required" for="name">First Name</label>
                    <input type="text"
                           class="form-control @error('name') is-invalid @enderror"
                           placeholder="Jone"
                           name="name" id="name"
                           value="{{ old('name', @$user->name)}}"/>

                    @error('name')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                </div>

            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label class="required" for="lname">Last Name</label>
                    <input type="text" class="form-control @error('lname') is-invalid @enderror"
                           placeholder="Dove"
                           name="lname" id="lname"
                           value="{{ old('lname', @$user->lname)}}"/>

                    @error('lname')
                    <div class="badge badge-pill badge-light-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label class="required" for="nic">NIC Number</label>
                    <input id="nic" type="text"
                           class="form-control @error('nic') is-invalid @enderror"
                           name="nic"
                           placeholder="991234567V"
                           value="{{ old('nic', @$user->nic)}}"/>

                    @error('nic')
                    <div class="badge badge-pill badge-light-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label class="required" for="gender">Gender</label>
                    <select class="form-control @error('gender') is-invalid @enderror" name="gender" id="gender">
                        <option value="None" {{ old('gender', @$user->gender) === 'None' ? 'selected' : '' }}>None
                        </option>
                        <option value="Male" {{ old('gender', @$user->gender) === 'Male' ? 'selected' : '' }}>Male
                        </option>
                        <option value="Female" {{ old('gender', @$user->gender) === 'Female' ? 'selected' : '' }}>
                            Female
                        </option>
                        <option value="Other" {{ old('gender', @$user->gender) === 'Other' ? 'selected' : '' }}>Other
                        </option>
                    </select>

                    @error('gender')
                    <div class="badge badge-pill badge-light-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="form-group">
                    <label class="required" for="contact">Contact Number</label>
                    <input id="contact" type="text"
                           class="form-control @error('contact') is-invalid @enderror"
                           placeholder="+94719874563"
                           name="contact"
                           value="{{ old('contact', @$user->contact)}}"/>
                    @error('contact')
                    <div class="badge badge-pill badge-light-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label class="" for="email">E-mail</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                           placeholder="example@gmail.com"
                           name="email" id="email" value="{{ old('email', @$user->email)}}"/>
                    @error('email')
                    <div class="badge badge-pill badge-light-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>


            <div class="col-12">
                <h4 class="mb-1 mt-2">
                    <i data-feather="map-pin" class="font-medium-4 mr-25"></i>
                    <span class="align-middle">Address</span>
                </h4>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label class="required" for="address_line_1">No</label>
                    <input id="no" type="text"
                           class="form-control @error('no') is-invalid @enderror"
                           placeholder="A-65" name="no"
                           value="{{ old('no', @$user->no)}}"/>
                    @error('no')
                    <div class="badge badge-pill badge-light-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label for="street">Street</label>
                    <input id="street" type="text" class="form-control"
                           placeholder=" Groove Street" name="street"
                           value="{{ old('street', @$user->street)}}"/>
                    @error('street')
                    <div class="badge badge-pill badge-light-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label class="required" for="city">City</label>
                    <input id="city" type="text"
                           class="form-control @error('city') is-invalid @enderror"
                           placeholder="New York"
                           name="city" value="{{ old('city', @$user->city)}}"/>
                    @error('city')
                    <div class="badge badge-pill badge-light-danger">{{$message}}</div>
                    @enderror
                </div>
            </div>

            <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                    Changes
                </button>
                <button type="reset" class="btn btn-outline-secondary">Reset</button>
            </div>
        </div>
    </form>


<div class="mt-4">
    <h4>User Order List</h4>

    <div class="container">
        <table class="table table-hover table table-scroll">
            <thead style="width: auto">
            <tr>
                <th>ID</th>
                <th>Price</th>
                <th>Order Date</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->price}}</td>
                    <td>{{$order->order_date}}</td>
                    <td>
                        <button type="button" class=" btn btn-sm btn-primary" data-toggle="modal"
                                data-target="#productModal-{{$order->id}}">
                            View Payments
                        </button>

                        <div id="productModal-{{$order->id}}" class="modal">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">User Order Payments</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body" style="max-height: calc(50vh - 100px);overflow-y: auto;">
                                        <table class="table table-hover table table-scroll">
                                            <thead style="width: auto">
                                            <tr>
                                                <th>ID</th>
                                                <th>Amount</th>
                                                <th>Payment Method</th>
                                                <th>Payment Date</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($order->Payments as $payment)
                                                <tr>
                                                    <td>{{$payment->id}}</td>
                                                    <td>{{$payment->amount}}</td>
                                                    <td>{{$payment->payment_method}}</td>
                                                    <td>{{$payment->payment_date}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection
