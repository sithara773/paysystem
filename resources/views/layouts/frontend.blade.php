@extends('layouts.app')
@section('contents')
<div class="container">

    <div class="wrapper d-flex align-items-stretch">
        @include('layouts.sidebar')
        <div id="content" class="p-4 p-md-5">
            @include('layouts.header')


            <div class="content-body">
                <main class="py-4">
                    @yield('content')
                </main>
            </div>
        </div>

    </div>
</div>
@endsection
