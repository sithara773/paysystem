<nav id="sidebar">
    <div class="p-4 pt-5">
        <h2>
            <span>Task App</span>
        </h2>
        <ul class="list-unstyled components mb-5">
            <li class="active">
                <a href="{{route('home')}}">Home</a>
            </li>
            <li>
                <a href="#userSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Users</a>
                <ul class="collapse list-unstyled" id="userSubmenu">
                    <li>
                        <a href="{{route('userList')}}">User List</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#orderSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Orders</a>
                <ul class="collapse list-unstyled" id="orderSubmenu">
                    <li>
                        <a href="{{route('orderList')}}">Order List</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#paymentSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Payments</a>
                <ul class="collapse list-unstyled" id="paymentSubmenu">
                    <li>
                        <a href="{{route('payList')}}">Payment List</a>
                    </li>
                </ul>
            </li>

{{--            <li>--}}
{{--                <a href="#">Test Tab1</a>--}}
{{--            </li>--}}
{{--            <li>--}}
{{--                <a href="#">Test Tab2</a>--}}
{{--            </li>--}}
        </ul>


    </div>
</nav>
