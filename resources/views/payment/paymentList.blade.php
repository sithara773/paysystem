@extends('layouts.frontend')

@section('content')
    @if(session('success'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('success') }}
        </div>
    @elseif(session('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('error') }}
        </div>

    @endif


    <div class="d-flex justify-content-end mb-4">

        <form action="{{ route('importCSV') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <input class="form-control w-50 h-25 mr-3" type="file" name="file" accept=".csv">
                <button class="form-control btn btn-primary w-25" type="submit">Import CSV</button>
            </div>
        </form>

        <a type="button" class="form-control btn btn-secondary w-25 h-25" href="{{route('exportCSV')}}">Export Payments CSV</a>

    </div>



    <h2 class="d-flex justify-content-center">Payment List</h2>

    <div class="container" style="height: 500px; overflow-y: auto;">
        <table class="table table-hover table table-scroll">
            <thead style="width: auto">
            <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Amount</th>
                <th>Payment Method</th>
                <th>Payment Date</th>
                <th>Order Id</th>
            </tr>
            </thead>
            <tbody>

            @foreach($payments as $payment)
                <tr>
                    <td>{{$payment->id}}</td>
                    <td>{{$payment->order->user->name}}</td>
                    <td>{{$payment->amount}}</td>
                    <td>{{$payment->payment_method}}</td>
                    <td>{{$payment->payment_date}}</td>
                    <td>{{$payment->order_id}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script>
        function importCSV() {
            window.location = '/create/csv';
        }

    </script>


@endsection
