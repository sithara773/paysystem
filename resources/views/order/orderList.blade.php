@extends('layouts.frontend')

@section('content')

    <h2 class="d-flex justify-content-center">Order List</h2>

    <div class="container" style="height: 500px; overflow-y: auto;">
        <table class="table table-hover table table-scroll">
            <thead style="width: auto">
            <tr>
                <th>ID</th>
                <th>User</th>
                <th>User Email</th>
                <th>Price</th>
                <th>Order Date</th>
{{--                <th>status</th>--}}
            </tr>
            </thead>
            <tbody >
            @foreach($orders as $order)
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->user->name}}</td>
                    <td>{{$order->user->email}}</td>
                    <td>{{$order->price}}</td>
                    <td>{{$order->order_date}}</td>
{{--                    <td>{{$order->status}}</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


@endsection
