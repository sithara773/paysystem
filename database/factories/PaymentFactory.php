<?php

namespace Database\Factories;

use App\Models\order;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\payment>
 */
class PaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'amount'=>$this->faker->numberBetween(100, 10000),
            'payment_method'=>$this->faker->randomElement(['Cash','Credit Card','Paypal','BTC']),
            'payment_date'=>$this->faker->dateTimeThisMonth(),
            'order_id'=>order::factory(),
        ];
    }
}
