<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\order>
 */
class OrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'price'=>$this->faker->numberBetween(100, 10000),
            'status'=>$this->faker->randomElement(['Active','Deactive','Pending']),
            'order_date'=>$this->faker->dateTimeThisMonth(),
            'user_id'=>User::factory(),
        ];
    }
}
