<?php

namespace Database\Seeders;

use App\Models\order;
use App\Models\payment;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
       $orders= order::factory()->count(10)->create();
        foreach ($orders as $order) {
            $payments = rand(1, 3);
            for ($i = 0; $i < $payments; $i++) {
                payment::factory()->create(['order_id' => $order->id]);
            }
        }

    }
}
