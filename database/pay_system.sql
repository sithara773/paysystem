-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2023 at 07:39 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pay_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(140, '2014_10_12_000000_create_users_table', 1),
(141, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(142, '2014_10_12_100000_create_password_resets_table', 1),
(143, '2019_08_19_000000_create_failed_jobs_table', 1),
(144, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(145, '2023_10_29_114115_orders', 1),
(146, '2023_10_29_114131_payments', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `price`, `status`, `user_id`, `order_date`, `created_at`, `updated_at`) VALUES
(1, 1595, 'Deactive', 1, '2023-10-27', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(2, 3181, 'Active', 7, '2023-10-29', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(3, 9643, 'Active', 8, '2023-10-02', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(4, 2902, 'Active', 9, '2023-10-01', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(5, 1714, 'Active', 1, '2023-10-30', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(6, 3810, 'Deactive', 11, '2023-10-27', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(7, 2576, 'Deactive', 12, '2023-10-29', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(8, 5213, 'Active', 13, '2023-10-03', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(9, 5594, 'Pending', 1, '2023-10-10', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(10, 3868, 'Active', 15, '2023-10-21', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(11, 4365, 'Pending', 16, '2023-10-02', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(12, 4259, 'Deactive', 17, '2023-10-08', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(13, 9528, 'Pending', 18, '2023-10-15', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(14, 8214, 'Pending', 19, '2023-10-03', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(15, 7842, 'Active', 20, '2023-10-16', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(16, 4062, 'Active', 21, '2023-10-30', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(17, 3323, 'Pending', 1, '2023-10-17', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(18, 8610, 'Deactive', 23, '2023-10-29', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(19, 5420, 'Pending', 24, '2023-10-08', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(20, 8226, 'Active', 25, '2023-10-24', '2023-10-31 01:53:12', '2023-10-31 01:53:12');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `amount`, `payment_method`, `payment_date`, `order_id`, `created_at`, `updated_at`) VALUES
(361, 9163, 'Cash', '2023-10-10', 1, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(362, 911, 'Paypal', '2023-10-29', 2, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(363, 795, 'Cash', '2023-10-11', 2, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(364, 6141, 'BTC', '2023-10-10', 3, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(365, 3936, 'Credit Card', '2023-10-12', 3, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(366, 8610, 'Credit Card', '2023-10-18', 3, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(367, 3076, 'Paypal', '2023-10-04', 4, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(368, 6193, 'BTC', '2023-10-10', 4, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(369, 1498, 'Credit Card', '2023-10-30', 5, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(370, 9936, 'Credit Card', '2023-10-11', 5, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(371, 2272, 'BTC', '2023-10-09', 5, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(372, 7209, 'BTC', '2023-10-19', 6, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(373, 8123, 'Cash', '2023-10-26', 6, '2023-10-31 13:31:21', '2023-10-31 13:31:21'),
(374, 7682, 'Paypal', '2023-10-02', 7, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(375, 7542, 'BTC', '2023-10-23', 8, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(376, 326, 'BTC', '2023-10-08', 8, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(377, 5703, 'BTC', '2023-10-10', 8, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(378, 1676, 'Cash', '2023-10-23', 9, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(379, 6288, 'Cash', '2023-10-11', 10, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(380, 2964, 'Paypal', '2023-10-27', 10, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(381, 9579, 'Credit Card', '2023-10-19', 11, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(382, 2150, 'BTC', '2023-10-03', 12, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(383, 1066, 'Credit Card', '2023-10-10', 13, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(384, 7775, 'Credit Card', '2023-10-14', 14, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(385, 6321, 'Cash', '2023-10-16', 15, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(386, 4114, 'Cash', '2023-10-24', 16, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(387, 3964, 'Paypal', '2023-10-14', 17, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(388, 885, 'BTC', '2023-10-25', 18, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(389, 1933, 'Credit Card', '2023-10-30', 19, '2023-10-31 13:31:22', '2023-10-31 13:31:22'),
(390, 5779, 'Paypal', '2023-10-13', 20, '2023-10-31 13:31:22', '2023-10-31 13:31:22');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `nic` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `no` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lname`, `nic`, `email`, `contact`, `gender`, `no`, `street`, `city`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ines Mosciski', 'Lily Goldner', '7282410800', 'ezboncak@example.org', '+1-341-552-7087', 'Femail', '8861', 'Koelpin Spurs', 'Eldredchester', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'oPKfXkZP7s', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(2, 'Prof. Roma Roob III', 'Dr. Malinda Rempel DVM', '1974163830', 'lzboncak@example.net', '713-963-2735', 'Other', '90350', 'Petra Bypass', 'East Mallieberg', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '0g6S4jpfkK', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(3, 'Prof. Giovanna Gorczany', 'Devyn Murray', '7826608698', 'rharber@example.com', '740.668.7296', 'Other', '63841', 'Kuhlman Ranch', 'Jacklynstad', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3hVqBLAz6C', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(4, 'Angelica Boyle', 'Loma Jacobson DDS', '3665404777', 'zemlak.mallie@example.net', '1-507-248-6785', 'Male', '433', 'Heidenreich Radial', 'North Bobbieland', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YvW0iMILuA', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(5, 'Dorothea Hettinger', 'Sylvia Cormier', '2468620700', 'ipurdy@example.net', '(772) 985-2973', 'Male', '347', 'Rolfson Locks', 'Pagacside', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ktsp9Ovpx3', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(6, 'Cecile Larson', 'Muhammad Mann', '2127377454', 'kfeest@example.com', '(323) 477-7651', 'Other', '71367', 'Parker Mission', 'Greenfelderport', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'lW3fKOE4gb', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(7, 'Demond Grady V', 'Branson Miller', '9004051164', 'zkling@example.org', '1-580-810-2142', 'Other', '5117', 'Patsy Mountains', 'Ashlynnville', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'FcMOMmbjwr', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(8, 'Cordell DuBuque', 'Matilde Lemke', '6487316545', 'delaney.cruickshank@example.org', '1-229-779-3226', 'Other', '14209', 'Vern Loop', 'Lake Blake', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ynS262Ukjz', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(9, 'Nichole Muller', 'Nikko Mraz PhD', '1286826860', 'lynch.mya@example.com', '+1.443.993.3908', 'Femail', '4917', 'Antwan Knolls', 'Erdmanmouth', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'x9DWGujjX0', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(10, 'Kaylie Rippin', 'Alden Predovic', '5607520223', 'carmel.bins@example.com', '+1.256.965.1482', 'Other', '2244', 'Gislason Groves', 'New Norbert', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '4Fy1uIDn4G', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(11, 'Eldora Balistreri', 'Prof. Einar McKenzie', '9772125338', 'steuber.eden@example.org', '+1-986-622-6537', 'Femail', '731', 'Bart Plain', 'North Mervinville', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '7L334mUSXQ', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(12, 'Scarlett Heidenreich', 'Darrick Jones', '5830592053', 'shanna42@example.com', '+1-929-993-1723', 'Male', '876', 'Crystel Club', 'North Zeldaton', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'LqJX2PoNfd', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(13, 'Dr. Clare Marks Jr.', 'Burdette Vandervort III', '1344530523', 'ashton26@example.com', '1-731-807-7743', 'Other', '83486', 'Raul Ridges', 'Lake Brainview', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5On1l57Xs0', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(14, 'Mckenzie Marks', 'Angie Friesen MD', '4403377858', 'dexter71@example.com', '+1 (315) 530-7339', 'Femail', '2401', 'Kuhn Overpass', 'Wilkinsonshire', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ak2Y6PJige', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(15, 'Ms. Aracely Stokes', 'Josh Reichel DDS', '3255627501', 'justina28@example.org', '661-943-8072', 'Femail', '178', 'Tara Trace', 'Rogahnview', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'qhaec4F8WI', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(16, 'Jaqueline Rau', 'Meaghan Kris DDS', '4112609366', 'albina.mosciski@example.org', '+1-636-420-3608', 'Other', '5913', 'Rosella Meadow', 'Lake Mariannaton', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'tnr0hBBZxJ', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(17, 'Prof. Zora Kertzmann PhD', 'Miss Dina Lang', '1423316666', 'thuels@example.org', '+1.463.321.1127', 'Femail', '328', 'Hill Fords', 'Warrenshire', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'dyg7xVlQmU', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(18, 'Liliana Medhurst II', 'Christop Will DDS', '5504136582', 'harvey.wayne@example.com', '(718) 494-7490', 'Femail', '321', 'Hilpert Curve', 'Lake Meggiefort', '2023-10-31 01:53:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yXnjbOESC4', '2023-10-31 01:53:11', '2023-10-31 01:53:11'),
(19, 'Bernard Lemke Jr.', 'Bettie Graham', '9877983954', 'ova78@example.org', '+18284165069', 'Other', '685', 'Trudie Turnpike', 'Malikaside', '2023-10-31 01:53:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'ZT6W7zKEp3', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(20, 'Orin Hermann IV', 'Albin Blanda', '8653080935', 'garrison44@example.net', '+1.540.846.8165', 'Other', '592', 'Brown Highway', 'East Albina', '2023-10-31 01:53:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'xJEmq1xSTm', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(21, 'Collin Murray MD', 'Demarco Mosciski', '9919647182', 'randal.dach@example.org', '323-259-1983', 'Male', '357', 'Kohler Crossing', 'Dockshire', '2023-10-31 01:53:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'YTdNpGYzmL', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(22, 'Mr. Tremaine Simonis DDS', 'Mac Renner IV', '4838275505', 'micah74@example.net', '1-541-594-7413', 'Other', '7836', 'Reyes Ways', 'West Glenna', '2023-10-31 01:53:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'vtNmEfFNwT', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(23, 'Ben Koch DDS', 'Mary Corwin', '6202632031', 'ortiz.raul@example.com', '(907) 531-0827', 'Femail', '4834', 'Klein Lake', 'Santaton', '2023-10-31 01:53:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'DhYpOBMQFN', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(24, 'Mr. Sage Gerhold DDS', 'Kristofer Cremin', '1185498707', 'keven.hyatt@example.net', '954.462.2285', 'Male', '9945', 'Jensen Point', 'New Melvinahaven', '2023-10-31 01:53:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'XArOcETMnV', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(25, 'Ignacio Barton', 'Else McDermott', '9237012497', 'bechtelar.richmond@example.net', '+15597944574', 'Femail', '11403', 'Hackett Pines', 'Davisview', '2023-10-31 01:53:12', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'rX1VM3qwXs', '2023-10-31 01:53:12', '2023-10-31 01:53:12'),
(26, 'sithara', NULL, NULL, 'sitharalakshan773@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, '$2y$10$.5vypNvkA5DJ0.BqoOcWROwpPV2TaUKaThT4ZK/I8UkJhVI1lpmWe', NULL, '2023-10-31 05:06:21', '2023-10-31 05:06:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_order_id_foreign` (`order_id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=391;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
